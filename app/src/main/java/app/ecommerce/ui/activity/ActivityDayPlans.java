package app.ecommerce.ui.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import app.ecommerce.ui.R;
import app.ecommerce.ui.adapter.PlansAdapter;
import app.ecommerce.ui.data.DataGenerator;
import app.ecommerce.ui.model.PlansPhoto;
import app.ecommerce.ui.utils.Tools;

public class ActivityDayPlans extends AppCompatActivity {

    LinearLayout mainLinear;
    Button imgArrow;
    CardView mainCard;
    RecyclerView recyclerView;
    PlansAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_plans);

        initToolbar();
        initExpandButton();
        initRecyclerSuggest();
        initRecyclerMain();
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        //change menu button color
        Tools.changeNavigationIconColor(toolbar, getResources().getColor(R.color.grey_60));
        //change overflow menu button color
        Tools.changeOverflowMenuIconColor(toolbar, getResources().getColor(R.color.grey_60));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Day Plans");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this, R.color.grey_5);
        Tools.setSystemBarLight(this);
    }

    public void initExpandButton(){
        mainLinear = findViewById(R.id.ln_expand);
        imgArrow = findViewById(R.id.img_main_arrow);
        mainCard = findViewById(R.id.cv_main);

        imgArrow.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                if (mainLinear.getVisibility() == View.GONE){
                    TransitionManager.beginDelayedTransition(mainCard, new AutoTransition());
                    mainLinear.setVisibility(View.VISIBLE);
                    imgArrow.setBackgroundResource(R.drawable.ic_arrow_top);
                } else{
                    TransitionManager.beginDelayedTransition(mainCard, new AutoTransition());
                    mainLinear.setVisibility(View.GONE);
                    imgArrow.setBackgroundResource(R.drawable.ic_arrow_bottom);
                }
            }
        });
    }

    public void initRecyclerSuggest(){
        //init RecyclerviewSuggest
        recyclerView = findViewById(R.id.rv_suggest);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        ArrayList<PlansPhoto> list = DataGenerator.getPlans(this);

        adapter = new PlansAdapter(this, list);
        recyclerView.setAdapter(adapter);
    }

    public void initRecyclerMain(){
        //init RecyclerviewMain
        recyclerView = findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        ArrayList<PlansPhoto> list = DataGenerator.getPlans(this);

        adapter = new PlansAdapter(this, list);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.day_plans_menu, menu);
        Tools.changeMenuIconColor(menu, getResources().getColor(R.color.grey_60));
        return true;
    }
}
