package app.ecommerce.ui.model;

import android.widget.ImageView;

public class PlansPhoto {
    public int imgPhoto;

    public int getImgPhoto() {
        return imgPhoto;
    }

    public void setImgPhoto(int imgPhoto) {
        this.imgPhoto = imgPhoto;
    }
}
