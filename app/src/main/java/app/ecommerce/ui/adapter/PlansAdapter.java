package app.ecommerce.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.GifRequestBuilder;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import app.ecommerce.ui.R;
import app.ecommerce.ui.model.PlansPhoto;
import app.ecommerce.ui.utils.Tools;

public class PlansAdapter extends RecyclerView.Adapter<PlansAdapter.PlansViewHolder> {
    private ArrayList<PlansPhoto> list;

    private Context ctx;

    public PlansAdapter(Context context, ArrayList<PlansPhoto> list) {
        this.list = list;
        ctx = context;
    }

    @NonNull
    @Override
    public PlansViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.day_plans_item, parent, false);
        return new PlansViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PlansViewHolder holder, int position) {
        Glide.with(holder.itemView.getContext())
                .load(list.get(position).getImgPhoto())
                .into(holder.imgPlans);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class PlansViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgPlans;
        public PlansViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPlans = itemView.findViewById(R.id.img_item);
        }
    }
}
